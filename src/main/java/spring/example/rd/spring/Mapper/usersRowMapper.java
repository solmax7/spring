package spring.example.rd.spring.Mapper;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import spring.example.rd.spring.mydto.User;

import java.sql.ResultSet;
import java.sql.SQLException;

//import javax.swing.tree.RowMapper;


@Component
public class usersRowMapper implements RowMapper {


    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User(resultSet.getInt("id"),
                resultSet.getString("name"), resultSet.getString("login"), resultSet.getString("password"), resultSet.getBoolean("role"));
        return user;
    }


}
