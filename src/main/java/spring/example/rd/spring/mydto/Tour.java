package spring.example.rd.spring.mydto;

public class Tour implements ITour {

    private String name;
    private Country country;
    private boolean burning;
    private int price;

    public Tour(String name, Country country, boolean burning, int price){

        this.name = name;
        this.country = country;
        this.burning = burning;
        this.price = price;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public boolean isBurning() {
        return burning;
    }

    public void setBurning(boolean burning) {
        this.burning = burning;
    }

  public String toString(){

        return "name: " + name + " country: " + country + " burning: " + burning + " price: " + price;

  }

    
}
