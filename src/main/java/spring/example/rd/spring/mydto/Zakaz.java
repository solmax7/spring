package spring.example.rd.spring.mydto;

public class Zakaz {

    private ITour tour;
    private int id;
    private IUser user;


    public ITour getTur() {
        return tour;
    }

    public void setTur(ITour tour) {
        this.tour = tour;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public IUser getUser() {
        return user;
    }

    public void setUser(IUser user) {
        this.user = user;
    }

    public Zakaz(int id, ITour tour, IUser user){

        this.id = id;
        this.tour = tour;
        this.user = user;

    }

    public String toString(){

        return "id:" + id + " tour: " + tour + " user: " + user;
    }



}
