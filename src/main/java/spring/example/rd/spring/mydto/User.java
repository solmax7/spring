package spring.example.rd.spring.mydto;

public class User {

    private String name;
    private int id;
    private boolean role;
    private String login;
    private String password;

    public User(int id, String name, String login, String password, boolean role) {
    }

    public boolean isRole() {
        return role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setRole(boolean role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User(String name, int id, boolean role){

        this.name = name;
        this.role = role;
        this.id = id;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString(){

        return "id: " + id + ".name: " + name + ".role: " + role;

    }
}
