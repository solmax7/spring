package spring.example.rd.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import spring.example.rd.spring.service.Demo;

@SpringBootApplication
public class Application {

//	6. Система Турагентство. Заказчик выбирает и оплачивает Тур (отдых, экскурсия, шоппинг), при этом
//	создается Заказ. Турагент определяет тур как «горящий», размеры скидок постоянным клиентам

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(Application.class, args);

		//ApplicationContext ctx = SpringApplication.run(PracticeTaskSpringCore1Application.class, args);
		//MyNewUser user = ctx.getBean(MyNewUser.class);
        Demo demo = ctx.getBean(Demo.class);
        demo.execute();

		//System.out.println(user.GetName());
		//bean.doSomething(); // Call bean’s methods
	}
}

