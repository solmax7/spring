package spring.example.rd.spring.service;

import spring.example.rd.spring.mydto.Country;
import spring.example.rd.spring.mydto.Tour;

public class TourService {


        public static Tour[] tour = new Tour[]{
                new Tour("Тур в Египет за 100$", Country.EGYPT, true, 100),
                new Tour ("Тур в Италию за 1000$", Country.ITALY, false, 1000),
                new Tour("Тур в Испанию за 800$", Country.SPAIN, false, 800)
        };

}
