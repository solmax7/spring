package spring.example.rd.spring.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import spring.example.rd.spring.Mapper.usersRowMapper;
import spring.example.rd.spring.mydto.IUser;
import spring.example.rd.spring.mydto.User;

import java.util.List;

@Repository
public class usersSql {

        @Autowired
        JdbcTemplate jdbcTemplate;

        @Autowired
        usersRowMapper Row;

        //@Override
        public List<User> getByUsers() {
            String sql = "SELECT * FROM users";
            return  jdbcTemplate.query(sql, Row::mapRow);
        }

        public void addUser(IUser user) {
            String sql = "INSERT INTO users (name, , login, password) " +
                    "VALUES (?, ?, ?, ?)";
            jdbcTemplate.update(sql, user.getName(), user.getId(), user.getRole());
        }

}



