package spring.example.rd.spring.repository;

import org.springframework.stereotype.Service;
import spring.example.rd.spring.mydto.Tour;
import spring.example.rd.spring.service.TourService;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Service
public class Tours {

    // @Autowired
    private Set<Tour> tours = new HashSet<>();

    public  void addTour(Tour tour){

        tours.add(tour);

    }

    public void addTour() {
        tours.addAll(Arrays.asList(TourService.tour));
    }



    @Override
    public String toString(){

        return (String) Arrays.toString(new Set[]{tours});
    }

}
